package sukhan.mycompany.entities;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "root")
public class Root {
    private List<User> users;
}
