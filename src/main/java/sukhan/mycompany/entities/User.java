package sukhan.mycompany.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@XmlRootElement(name = "id")
public class User implements Serializable {
    private String id = UUID.randomUUID().toString();
    @XmlElement(name = "Login")
    private String login;
    @XmlElement(name = "address")
    private String email;

    public User(String login, String email) {
        this.login = login;
        this.email = email;
    }

    public String getId() {
        return id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}
