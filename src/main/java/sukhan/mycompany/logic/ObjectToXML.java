package sukhan.mycompany.logic;

import sukhan.mycompany.entities.User;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.OutputStream;

public class ObjectToXML {
    public void start () throws JAXBException {
        final InputStream in = ClassLoader.getSystemResourceAsStream("");
        final JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        final User user = new User("sukhan_ei", "my_mail@mail.com");
        jaxbMarshaller.marshal(user, System.out);
    }
}
