package sukhan.mycompany;

import sukhan.mycompany.entities.User;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

/**
 * rO0ABXNyAB5zdWtoYW4ubXljb21wYW55LmVudGl0aWVzLlVzZXIAAAAAAAAAAQIAA0wABWVtYWls
 * dAASTGphdmEvbGFuZy9TdHJpbmc7TAACaWR0ABNMamF2YS9sYW5nL0ludGVnZXI7TAAFbG9naW5x
 * AH4AAXhwdAAQbXlfbWFpbEBtYWlsLmNvbXB0AAlzdWtoYW5fZWk=
 */
public class App 
{
    public static void main( String[] args ) throws IOException, JAXBException {
        final User user = new User("sukhan_ei", "my_mail@mail.com");
        final ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
        objectOutputStream.writeObject(user);
        objectOutputStream.close();

        final byte[] bytes = byteOutputStream.toByteArray();
        System.out.println(new String(bytes));
        System.out.println();

        final String base64 = new BASE64Encoder().encode(bytes);
        System.out.println(base64);
        System.out.println();

        objectOutputStream.close();

//        final ObjectInputStream objectInputStream = new ObjectInputStream();
//        final User user1 = objectInputStream.read();

        final InputStream in = ClassLoader.getSystemResourceAsStream("");
        final JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.marshal(user, System.out);
    }
}
